from django.template import Library
from django.template.defaultfilters import stringfilter

from urllib.parse import quote_plus

register = Library()


@register.filter(is_safe=True)
@stringfilter
def urlencode_plus(value, safe=None):
	"""
	Like built-in urlencode, but spaces are encoded with plus signs.
	"""
	kwargs = {}
	if safe is not None:
		kwargs['safe'] = safe
	return quote_plus(value, **kwargs)
